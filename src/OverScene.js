var GameOver = cc.LayerColor.extend({
    init: function(score){
        this._super( new cc.Color( 0, 0, 0, 0 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        
        this.score = score;
        
        this.showOverLabel();
        this.showScore();
        this.createReplayButton();
        this.createBackButton();
    },
    
    createReplayButton: function() {
        this.replayButtonItem = new cc.MenuItemImage(
            res.replayButton_png,
            res.replayButton2_png,
            function () {
                cc.director.runScene( new StartScene() );
            }, this);
        this.replayButton = new cc.Menu( this.replayButtonItem );
        this.replayButton.setPosition( new cc.Point( 300, 150 ) );
        this.addChild( this.replayButton );
    },
    
    createBackButton: function() {
        this.backButtonItem = new cc.MenuItemImage(
            res.tomenuButton_png,
            res.tomenuButton2_png,
            function () {
                cc.director.runScene( new MenuScene() );
            }, this);
        this.backButton = new cc.Menu( this.backButtonItem );
        this.backButton.setPosition( new cc.Point( 550, 150 ) );
        this.addChild( this.backButton );
    },
    
    showOverLabel: function() {
        this.OverLabel = cc.LabelTTF.create( 'Game Over' ,'Arial',50);
        this.OverLabel.setColor(cc.color(255, 255, 255));
        this.OverLabel.setPosition( new cc.Point( screenWidth / 2 , screenHeight / 2 ) );
        this.addChild( this.OverLabel, 2 );
    },
    
    showScore: function() {
        this.OverScore = cc.LabelTTF.create( 'Your Score : ' + this.score ,'Arial',20);
        this.OverScore.setColor(cc.color(255, 255, 255));
        this.OverScore.setPosition( new cc.Point( screenWidth / 2 , screenHeight / 2 - 50 ) );
        this.addChild( this.OverScore, 2 );
    }
    
});
var OverScene = cc.Scene.extend({
    ctor: function(score) {
        this._super();
        this.score = score;
    },
    
    onEnter: function() {
        this._super();
        var layer = new GameOver();
        layer.init(this.score);
        this.addChild( layer );
        
    }
});
