var Collectible = cc.Sprite.extend({
    ctor: function( fly ) {
        this._super();
        this.speed = 4;
        this.player = fly;
        this.randomPosition();
    },
    
    update: function( dt ) {
        this.move();
        this.removeAtEdge();
        this.hitPlayer();
    },
    
    randomPosition: function() {
        var ranY = 10 + Math.floor( Math.random() * ( screenHeight - 20 ));
        this.setPosition(new cc.Point( screenWidth , ranY ) );
    },
    
    move: function() {
        var pos = this.getPosition();
        this.setPosition(new cc.Point( pos.x - this.speed , pos.y ) );
    },
    
    removeAtEdge: function() {
        var pos = this.getPosition();
        if ( pos.x <= 0 ) {
            this.removeFromParent();   
        }
    },
    
    hitPlayer: function() {
        var myPos = this.getPosition();
        var pPos = this.player.getPosition();
        if ( ( Math.abs( myPos.x - pPos.x ) <= 16 ) &&
                ( Math.abs( myPos.y - pPos.y ) <= 16 ) ) {
            this.player.score += 100;
            this.removeFromParent();
        }
    }
    
});

var Poop = Collectible.extend({
    ctor: function( fly ) {
        this._super();
        this.player = fly;
        this.initWithFile( res.Poop_1_png );
        this.poopAction = this.setAnimation();
        this.runAction(this.poopAction);
    },
    
    setAnimation: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( res.Poop_1_png );
        animation.addSpriteFrameWithFile( res.Poop_2_png );
        animation.addSpriteFrameWithFile( res.Poop_3_png );
        animation.setDelayPerUnit( 0.1 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
});
                          