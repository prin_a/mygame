var GameLayer = cc.LayerColor.extend({
    init: function(){
        this._super( new cc.Color( 217, 255, 255, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        
        this.scoreTemp = -500;
        
        this.createBackground();
        this.initPlayer();
        this.showHP();
        this.showStamina();
        this.showMagicka();
        this.showScore();
        
        this.setupShieldFX();
        this.setupSpeedFX();
        this.setupBeamFX();
        
        this.addKeyboardHandlers();
        this.scheduleUpdate();
        
        this.counter = 0;
    },
    
    showHP: function(){
        this.HPbarMax = new HPBarMax();
        this.addChild( this.HPbarMax );
        this.HPbar = new HPBar();
        this.addChild( this.HPbar, 2 );
        this.HPLabel = cc.LabelTTF.create( 'HP      100 / 100' ,'Arial',20);
        this.HPLabel.setColor(cc.color(0, 0, 0));
        this.HPLabel.setPosition( new cc.Point( 80 , 570 ) );
        this.addChild( this.HPLabel, 2 );   
    },
    
    showStamina: function(){
        this.SMbarMax = new SMBarMax();
        this.addChild( this.SMbarMax, 2 );
        this.SMbar = new SMBar();
        this.addChild( this.SMbar, 2 );
        this.StaminaLabel = cc.LabelTTF.create( 'Stamina    20 / 20' ,'Arial',18);
        this.StaminaLabel.setColor(cc.color(0, 0, 0));
        this.StaminaLabel.setPosition( new cc.Point( 80 , 540 ) );
        this.addChild( this.StaminaLabel, 2 );  
    },
    
    showMagicka: function() {
        this.MPbarMax = new MPBarMax();
        this.addChild( this.MPbarMax, 2 );
        this.MPbar = new MPBar();
        this.addChild( this.MPbar, 2 );
        this.MPLabel = cc.LabelTTF.create( 'MP      100 / 100' ,'Arial',20);
        this.MPLabel.setColor(cc.color(0, 0, 0));
        this.MPLabel.setPosition( new cc.Point( 80 , 510 ) );
        this.addChild( this.MPLabel, 2 );  
    },
    
    showScore: function() {
        this.ScoreLabel = cc.LabelTTF.create( 'Score : 0' ,'Arial',20);  
        this.ScoreLabel.setColor(cc.color(100, 0, 0));
        this.ScoreLabel.setPosition( new cc.Point( screenWidth / 2 , 570 ) );
        this.addChild( this.ScoreLabel, 2 );
    },
    
    createBackground: function() {
        this.clouds = [];
        for ( var i = 0; i < 4; i++ ) { 
            var cloud = new Cloud();            
            this.addChild( cloud );
            cloud.setPosition(new cc.Point( ( i + 1 ) * 260 , ( i + 1 ) * 170 ) );
            if ( i == 3 ) {
                cloud.setPosition(new cc.Point( cloud.x , ( i - 1 ) * 170 ) );
            }
            cloud.scheduleUpdate();
            this.clouds.push( cloud );
        }
    },
    
    initPlayer: function() {
        this.player = new Player();
        this.player.setPosition( new cc.Point( screenWidth / 4, screenHeight / 2 ) );
        this.addChild( this.player , 1);
        this.player.scheduleUpdate();
    },
    
    setupShieldFX: function() {
        this.shieldFX = new Shield( this.player );
        this.addChild( this.shieldFX );
    },
    
    setupSpeedFX: function() {
        this.speedFX = new Speed( this.player );
        this.addChild( this.speedFX );
    },
    
    setupBeamFX: function() {
        this.beamFX = new Beam(this.player );
        this.addChild( this.beamFX );
    },

    onKeyDown: function( keyCode, event ) {
        if ( keyCode == cc.KEY.a ) 
            Player.KEYDOWN.A = true;
        if ( keyCode == cc.KEY.w ) 
            Player.KEYDOWN.W = true;
        if ( keyCode == cc.KEY.s ) 
            Player.KEYDOWN.S = true;
        if ( keyCode == cc.KEY.d ) 
            Player.KEYDOWN.D = true;
        if ( keyCode == cc.KEY.shift ) {
            this.player.speed = 6;
            Player.regenStamina = false;
        }
        
        if ( keyCode == cc.KEY.e ) {
            if ( !this.player.invuleneble ){
                if ( this.player.magicka > 5) {
                    this.player.invuleneble = true;
                    this.player.magicka -= 5;
                }
            }
        }
        if ( keyCode == cc.KEY.space ) {
            if ( !this.player.isBeam ){
                if ( this.player.magicka > 5) {
                    this.player.isBeam = true;
                    this.player.magicka -= 5;
                }
            }
        }
            
    },
    
    onKeyUp: function( keyCode, event ) {
        if ( keyCode == cc.KEY.a ) 
            Player.KEYDOWN.A = false;
        if ( keyCode == cc.KEY.w ) 
            Player.KEYDOWN.W = false;
        if ( keyCode == cc.KEY.s ) 
            Player.KEYDOWN.S = false;
        if ( keyCode == cc.KEY.d ) 
            Player.KEYDOWN.D = false;
        if ( keyCode == cc.KEY.shift ) {
            this.player.speed = 4;
            Player.regenStamina = true;
        }
        if ( keyCode == cc.KEY.e ) {
            this.player.invuleneble = false;   
        }
        if ( keyCode == cc.KEY.space ) {
            this.player.isBeam = false;  
        }
    },
    
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
    
    update: function( dt ) {
        this.count( dt )
        this.spawnUltraman();
        this.spawnPoop();
        this.updateHP();
        this.updateStamina();
        this.updateScore();
        this.updateMagicka();
    },
    
    count: function( dt ) {
        this.counter += dt;
    },
    
    spawnUltraman: function() {
        var ran = 0.5 + Math.floor( Math.random() * 0.5 );
        if ( this.counter >= ran ) {
            this.counter = 0;
            var ran = 1 + Math.floor( Math.random() * 3 );
            for ( var i = 0; i < ran; i++ ) {
                
                this.enemy = new Ultraman( this.player );
                this.addChild( this.enemy );
            }
        }     
    },
    
    spawnPoop: function() {
        if ( this.player.score - this.scoreTemp >= 300 ) {
            this.scoreTemp = this.player.score;
            var poop = new Poop( this.player );
            this.addChild( poop );
            poop.scheduleUpdate();
        }
    },
    
    updateHP: function() {
        if ( this.player.hp > 0 ) {
            this.HPLabel.setString( 'HP      ' + this.player.hp + ' / ' + Player.MAX_HP );
            this.HPbar.setScaleX( this.player.hp / Player.MAX_HP );
        } else {
            this.HPLabel.setString( 'HP      0 / ' + Player.MAX_HP );
            this.gameOver();
        }
    },
    
    updateStamina: function() {
        this.StaminaLabel.setString( 'Stamina    ' + this.player.stamina + ' / ' + Player.MAX_STAMINA );
        this.SMbar.setScaleX( this.player.stamina / Player.MAX_STAMINA );
    },
    
    updateMagicka: function() {
        this.MPLabel.setString( 'MP      ' + this.player.magicka + ' / ' + Player.MAX_MAGICKA );
        this.MPbar.setScaleX( this.player.magicka / Player.MAX_MAGICKA );
    },
    
    updateScore: function() {
        this.ScoreLabel.setString( 'Score : ' + this.player.score );  
    },
    
    gameOver: function() {
        this.player.resetPlayer();
        cc.director.runScene(new OverScene(this.player.score) );
    }
    
});
var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild( layer );
        
    }
});
