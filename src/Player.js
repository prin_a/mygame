var Player = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( res.Fly_2_png );
        this.direction = 0;
        this.hp = Player.MAX_HP;
        this.magicka = Player.MAX_MAGICKA;
        this.speed = 4;
        this.stamina = Player.MAX_STAMINA;
        this.invuleneble = false;
        this.score = 0;
        this.Animation = this.setAnimation();
        this.runAction(this.Animation);
        this.isBeam = false;
    },
    
    update: function( dt ) {
        this.stopAtEdge();
        this.move();    
        this.invulenebleTime( dt );
        this.outOfStamina( dt );
        this.regenStamina( dt );
        this.updateScore( dt );
        this.regen( dt );
        this.beamTime( dt );
    },
    
    setAnimation: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( res.Fly_1_png );
        animation.addSpriteFrameWithFile( res.Fly_2_png );
        animation.addSpriteFrameWithFile( res.Fly_3_png );
        animation.addSpriteFrameWithFile( res.Fly_2_png );
        animation.setDelayPerUnit( 0.05 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    
    move: function() {
        var pos = this.getPosition();
        var newX = pos.x;
        var newY = pos.y;
        if ( Player.KEYDOWN.A ) {
            newX -= this.speed;
        }
        if ( Player.KEYDOWN.W ) {
            newY += this.speed;
        }
        if ( Player.KEYDOWN.D ) {
            newX += this.speed;
        }
        if ( Player.KEYDOWN.S ) {
            newY -= this.speed;
        }
        this.setPosition(new cc.Point( newX , newY ));
    },
    
    outOfStamina: function( dt ) {
        if ( this.speed == 6 ) {
            if ( this.stamina <= 0 ) {
                this.speed = 4;
                Player.countStamina = 0;
                Player.regenStamina = true;
            } else {
                Player.countStamina += dt;
                if ( Player.countStamina >= 0.2 ) {
                    this.stamina -= 1;
                    Player.countStamina = 0;
                    Player.regenStamina = false;
                }
            }
        }
    },
    
    regenStamina: function( dt ) {
        if ( Player.regenStamina ) {           
            if ( Player.countStamina >= 3 ) {
                Player.countRegenStamina += dt;
                if ( Player.countRegenStamina >= 0.1 ) {
                    this.stamina += 1;
                    Player.countRegenStamina = 0;
                }
                if ( this.stamina >= Player.MAX_STAMINA ) {
                    Player.regenStamina = false;
                    Player.countStamina = 0;
                }
            } else Player.countStamina += dt;
        }
    },
    
    regen: function( dt ) {
        Player.countRegen += dt;
        if ( Player.countRegen >= 1 ) {
            Player.countRegen = 0;
            if ( this.hp < Player.MAX_HP ) {
                this.hp += 1;
            }
            if ( this.magicka < Player.MAX_MAGICKA - 2 ) {
                this.magicka += 3;   
            }
        }
    },
    
    stopAtEdge: function() {
        var pos = this.getPosition(); 
        if ( pos.x <= 0 ) {
            Player.KEYDOWN.A = false;
        } 
        if ( pos.x >= screenWidth ) {
            Player.KEYDOWN.D = false;
        }
        if ( pos.y <= 0 ) {
            Player.KEYDOWN.S = false;
        } 
        if ( pos.y >= screenHeight ) {
            Player.KEYDOWN.W = false;
        }
    },
    
    invulenebleTime: function( dt ) {
        if ( this.invuleneble ) {
            Player.countInvulenebleTime += dt
            if ( Player.countInvulenebleTime >= 0.3 ) {
                this.magicka -= 5;
                if ( this.magicka <0 ) {
                    this.magicka = 0;
                    this.invuleneble = false;
                }
                Player.countInvulenebleTime = 0;
            }
        }
    },
    
    beamTime: function( dt ) {
        if ( this.isBeam ) {
            Player.countBeam += dt
            if ( Player.countBeam >= 0.5 ) {
                this.magicka -= 5;
                if ( this.magicka <0 ) {
                    this.magicka = 0;
                    this.isBeam = false;
                }
                Player.countBeam = 0;
            }
        }
    },
    
    updateScore: function( dt ) {
        this.score += Math.floor( dt * 100 );
    },
    
    resetPlayer: function() {
        Player.KEYDOWN.A = false;
        Player.KEYDOWN.W = false;
        Player.KEYDOWN.D = false;
        Player.KEYDOWN.S = false;
        Player.countInvulenebleTime = 0;
        Player.countStamina = 0;
        Player.regenStamina = false;
        Player.countRegenStamina = 0;
        Player.countRegen = 0;
        Player.countBeam = 0;
    }
        
});
Player.KEYDOWN = {
    A : false,
    W : false,
    D : false,
    S : false
};
Player.MAX_HP = 100;
Player.MAX_STAMINA = 20;
Player.MAX_MAGICKA = 100;
Player.countBeam = 0;
Player.countInvulenebleTime = 0;
Player.countStamina = 0;
Player.countRegen = 0;
Player.regenStamina = false;
Player.countRegenStamina = 0;