var Background = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.speed = 1;
    },
    
    update: function( dt ) {
        this.move();
        this.loopAtEdge();
        
    },
    
    move: function() {
        var pos = this.getPosition();
        this.setPosition(new cc.Point( pos.x - this.speed , pos.y ) );
    },
    
    loopAtEdge: function() {
        var pos = this.getPosition();  
        if ( pos.x <= -150 ) {
            this.setPosition(new cc.Point( screenWidth + 150 , pos.y ) );
        }
    }
    
});

var Cloud = Background.extend({
    ctor: function() {
        this._super();
        this.initWithFile( res.Cloud_png );
    }
});
                          