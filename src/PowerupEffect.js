var Shield = cc.Sprite.extend({
    ctor: function( fly ) {
        this._super();
        this.player = fly;
        this.initWithFile( res.Shield_1_png );
        this.Animation = this.setAnimation();
        this.runAction(this.Animation);
        this.scheduleUpdate();
    },
    
    update: function() {
        if ( this.player.invuleneble ) {
            this.setPosition( this.player.getPosition() );
            this.setOpacity( 255 );
        } else this.setOpacity( 0 );
    },
    
    setAnimation: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( res.Shield_1_png );
        animation.addSpriteFrameWithFile( res.Shield_2_png );
        animation.addSpriteFrameWithFile( res.Shield_3_png );
        animation.addSpriteFrameWithFile( res.Shield_2_png );
        animation.setDelayPerUnit( 0.1 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
 
});

var Speed = cc.Sprite.extend({
    ctor: function( fly ) {
        this._super();
        this.player = fly;
        this.initWithFile( res.Speed_1_png );
        this.Animation = this.setAnimation();
        this.runAction(this.Animation);
        this.scheduleUpdate();
    },
    
    update: function() {
        if ( this.player.speed == 6 ) {
            this.setPosition( this.player.getPosition() );
            this.setOpacity( 255 );
        } else this.setOpacity( 0 );
    }, 
    
    setAnimation: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( res.Speed_1_png );
        animation.addSpriteFrameWithFile( res.Speed_2_png );
        animation.addSpriteFrameWithFile( res.Speed_3_png );
        animation.setDelayPerUnit( 0.05 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
 
});

var Beam = cc.Sprite.extend({
    ctor: function( fly ) {
        this._super();
        this.player = fly;
        this.initWithFile( res.beam1_png );
        this.Animation = this.setAnimation();
        this.runAction(this.Animation);
        this.scheduleUpdate();
        this._setAnchorX(0);
    },
    
    update: function() {
        if ( this.player.isBeam ) {
            this.setPosition( this.player.getPosition() );
            this.setOpacity( 255 );
        } else this.setOpacity( 0 );
    }, 
    
    setAnimation: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( res.beam1_png );
        animation.addSpriteFrameWithFile( res.beam2_png );
        animation.setDelayPerUnit( 0.05 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
});
