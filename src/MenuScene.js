var Startmenu = cc.LayerColor.extend({
    init: function(){
        this._super( new cc.Color( 0, 0, 0, 0 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
        
        this.showBackground();
        this.initPlayer();
        this.createStartButton();
    },
    
    showBackground() {
        this.bg = new Background();
        this.bg.setPosition( screenWidth / 2, screenHeight / 2 );
        this.addChild( this.bg );
    },
    
    createStartButton: function() {
        this.startButtonItem = new cc.MenuItemImage(
            res.startButton_png,
            res.startButton2_png,
            function () {
                cc.director.runScene( new StartScene() );
            }, this);
        this.startButton = new cc.Menu( this.startButtonItem );
        this.startButton.setPosition( screenWidth / 2, screenHeight / 2 );
        this.addChild( this.startButton );
    },
    
    initPlayer: function() {
        this.player = new Player();
        this.player.setPosition( new cc.Point( screenWidth / 4, screenHeight / 2 ) );
        this.addChild( this.player , 1);
        this.player.setScale(2,2);
    },
    
});
var MenuScene = cc.Scene.extend({    
    onEnter: function() {
        this._super();
        var layer = new Startmenu();
        layer.init();
        this.addChild( layer );
        
    }
});

var Background = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( res.title_png );
    }
});