var Ultraman = cc.Sprite.extend({
    ctor: function( fly ) {
        this._super();
        this.initWithFile( res.Ultraman_png );
        this.randomPosition();
        this.scheduleUpdate();
        
        this.player = fly;
        this.speed = 5 + Math.floor( Math.random() * 5 );
    },
    
    randomPosition: function(){
        var x = screenWidth;
        var y = 1 + Math.floor( Math.random() * screenHeight );
        this.setPosition(new cc.Point( x , y ) );
    },
    
    update: function( dt ) {
        this.move();
        this.removeAtEdge();
        this.hitBeam();
        this.hitPlayer();
    },
    
    move: function() {
        var pos = this.getPosition();
        var pPos = this.player.getPosition();
        var newX = pos.x - this.speed;
        var newY = pos.y;
        if ( pos.x > pPos.x ) {
            if ( pos.y > pPos.y ) {
                newY -= 1;
            } else newY += 1; 
        }
        this.setPosition( new cc.Point( newX , newY ) );
    },
    
    removeAtEdge: function() {
        var pos = this.getPosition();
        if ( pos.x <= 0 ) {
            this.removeFromParent();   
        }
    },
    
    hitPlayer: function() {
        if ( !this.player.invuleneble ) {
            var myPos = this.getPosition();
            var pPos = this.player.getPosition();
            if ( ( Math.abs( myPos.x - pPos.x ) <= 16 ) &&
                    ( Math.abs( myPos.y - pPos.y ) <= 16 ) ) {
                this.player.hp = this.player.hp - 30;
                this.removeFromParent();
            }
        }
    },
     
    hitBeam: function() {
        if ( this.player.isBeam ) {
            var myPos = this.getPosition();
            var pPos = this.player.getPosition();
            if ( ( myPos.x > pPos.x ) &&
                    ( Math.abs( myPos.y - pPos.y ) <= 16 ) ) {
                this.player.score += 20;
                this.removeFromParent();
            }
        }
    }
    
});