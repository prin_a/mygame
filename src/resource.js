var res = {
    Fly_1_png : 'res/images/Fly/fly1.png',
    Fly_2_png : 'res/images/Fly/fly2.png',
    Fly_3_png : 'res/images/Fly/fly3.png',
    Ultraman_png : 'res/images/Untitled.png',
    Speed_1_png : 'res/images/Speed/Speed_1.png',
    Speed_2_png : 'res/images/Speed/Speed_2.png',
    Speed_3_png : 'res/images/Speed/Speed_3.png',
    Poop_1_png : 'res/images/PoopAnim/poop_1.png',
    Poop_2_png : 'res/images/PoopAnim/poop_2.png',
    Poop_3_png : 'res/images/PoopAnim/poop_3.png',
    Shield_1_png : 'res/images/Shield/Shield_1.png',
    Shield_2_png : 'res/images/Shield/Shield_2.png',
    Shield_3_png : 'res/images/Shield/Shield_3.png',
    Cloud_png : 'res/images/Cloud.png',
    startButton_png : 'res/images/startButton.png',
    startButton2_png : 'res/images/startButton2.png',
    replayButton_png : 'res/images/replayButton.png',
    replayButton2_png : 'res/images/replayButton2.png',
    tomenuButton_png : 'res/images/tomenuButton.png',
    tomenuButton2_png : 'res/images/tomenuButton2.png',
    hpbar_png : 'res/images/hp.png',
    hpbar_max_png : 'res/images/hp_max.png',
    smbar_png : 'res/images/sm.png',
    smbar_max_png : 'res/images/sm_max.png',
    magicka_png : 'res/images/magicka.png',
    magicka_max_png : 'res/images/magicka_max.png',
    beam1_png : 'res/images/beam.png',
    beam2_png : 'res/images/beam2.png',
    title_png : 'res/images/title.png'
    
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}