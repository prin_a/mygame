var HPBar = cc.Sprite.extend({
    ctor: function() {
		this._super();
		this.initWithFile( res.hpbar_png );
        this.setPosition( new cc.Point( 50, 560) );
		this.setAnchorPoint( new cc.Point( 0, 0) );
	}
    
});

var HPBarMax = cc.Sprite.extend({
    ctor: function() {
		this._super();
		this.initWithFile( res.hpbar_max_png );
        this.setPosition( new cc.Point( 50, 560) );
		this.setAnchorPoint( new cc.Point( 0, 0) );
    }
});
                          
var SMBar = cc.Sprite.extend({
    ctor: function() {
		this._super();
		this.initWithFile( res.smbar_png );
        this.setPosition( new cc.Point( 85, 534) );
		this.setAnchorPoint( new cc.Point( 0, 0) );
    }
});
    
var SMBarMax = cc.Sprite.extend({
    ctor: function() {
		this._super();
		this.initWithFile( res.smbar_max_png );
        this.setPosition( new cc.Point( 85, 534) );
		this.setAnchorPoint( new cc.Point( 0, 0) );
    }
});

var MPBar = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( res.magicka_png );
        this.setPosition( new cc.Point( 50, 500) );
		this.setAnchorPoint( new cc.Point( 0, 0) );
    }
});

var MPBarMax = cc.Sprite.extend({
    ctor: function() {
		this._super();
		this.initWithFile( res.magicka_max_png );
        this.setPosition( new cc.Point( 50, 500) );
		this.setAnchorPoint( new cc.Point( 0, 0) );
    }
});
    
          